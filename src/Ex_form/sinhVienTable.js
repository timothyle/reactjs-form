import React, { Component } from 'react'


export default class SinhVienTable extends Component {
  renderSinhVienTable = () => {
    return this.props.dsSinhVien.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.sodienthoai}</td>
          <td>{item.email}</td>
          <td>
            <button className='btn btn-danger' onClick={()=>this.props.handleSinhVienRemove(item.id)}>Delete</button>
            <button className='btn btn-warning' onClick={() => {this.props.handleEditSinhVien(item,item.id)}}>Edit</button>  
          </td>
        </tr>
      )
    })
  }
  
  render() {
    return (
      <div>
        <div className="form-group w-50 d-flex">
          <input
            type="text"
            className="form-control"
            onChange={this.props.handleTimKiem}
            value={this.props.thongtin}
            placeholder="Tìm kiếm"
          />
          <button
              onClick={() => {
                this.props.handleTimKiemTTSinhVien(this.props.thongtin);
              }}
              className="btn btn-dark text-white ml-3"
              style={{ cursor: "pointer"}}
            >
              Search
          </button>
        </div>
      <table className='table'>
        <thead className='bg-dark text-white'>
          <tr>
            <th>Mã Sinh Viên</th>
            <th>Họ và Tên</th>
            <th>Số điện thoại</th>
            <th>Email</th>
          </tr>
        </thead>
        <tbody>{this.renderSinhVienTable()}</tbody>
      </table>

      </div>
    )
  }
}
