import React, { Component } from 'react'
import SVInputForm from './SinhVienInputForm'
import SinhVienTable from './sinhVienTable'

export default class EX_FORM extends Component {
  state = {
    dsSinhVien: [
      {
        id:111,
        name: "Nguyen Van A",
        sodienthoai:"0909123456",
        email:"koko@gmail.com"
    },
    {
      id: 222,
      name: "Nguyễn Văn Anh",
      sodienthoai: "0901233456",
      email: "anh@abc.vn",
    },
    {
      id: 333,
      name: "Trần Hoàng Hà My",
      sodienthoai: "0901111111",
      email: "hamy@gmail.com",
    }
  ],
  svEdited:null,
  thongtin:"",
}
handleAddSinhVien = (newSinhVien) => {
    let cloneDSSinhVien = [...this.state.dsSinhVien, newSinhVien];
    this.setState({ dsSinhVien: cloneDSSinhVien})
  }
  handleEditSinhVien = (value) => {
    this.setState({svEdited:value});
  }
  handleSinhVienRemove = (userId) => {
    let index = this.state.dsSinhVien.findIndex((item) =>{
      return item.id === userId;
    });
    if (index !== -1){
      let cloneDSSinhVien = [...this.state.dsSinhVien];
      cloneDSSinhVien.splice(index , 1);
      this.setState ({dsSinhVien: cloneDSSinhVien})
    }
  }
  handleCapNhatTTSinhVien = (userId, newSinhVien) => {
    let index = this.state.dsSinhVien.findIndex((item) => {
      return item.id === userId;
    });
    if (index !== -1) {
      let cloneDSSinhVien = [...this.state.dsSinhVien];
      cloneDSSinhVien[index] = newSinhVien;
      this.setState({ dsSinhVien: cloneDSSinhVien });
    }
  };
  handleTimKiem = (item) => {
    this.setState({ thongtin: item.target.value });
  };

  handleTimKiemTTSinhVien = (thongtin) => {
    let filterSV = [...this.state.dsSinhVien];
    let renderFilterList = filterSV.filter(
      (sv) =>
        sv.name.trim().toLowerCase().indexOf(thongtin.trim().toLowerCase()) !==
        -1
    );
    this.setState({ dsSinhVien: renderFilterList });
  };



  render() {
    return (
      <div className='container py-5'>
        <SVInputForm 
        svEdited ={this.state.svEdited} 
        handleCapNhatTTSinhVien={this.handleCapNhatTTSinhVien}
        handleAddSinhVien={this.handleAddSinhVien}/>
        <SinhVienTable
        handleEditSinhVien={this.handleEditSinhVien}
        handleCapNhatTTSinhVien={this.handleCapNhatTTSinhVien}
        handleSinhVienRemove={this.handleSinhVienRemove}
        handleTimKiem={this.handleTimKiem}
        handleTimKiemTTSinhVien={this.handleTimKiemTTSinhVien}
        thongtin={this.state.thongtin}
        dsSinhVien={this.state.dsSinhVien}/>
      </div>
    )
  }
}
