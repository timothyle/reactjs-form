import React, { Component } from 'react'

export default class SVInputForm extends Component {
  constructor(props) {
    super(props);
    this.inputRef = React.createRef();
  }
  state = {
    user: {
        id:"",
        name: "",
        sodienthoai:"",
        email:"",
    }
  }
  handleGetSinhVienForm = (e) => {
    let { value, name: key } = e.target;
    let cloneUser = { ...this.state.user }
    cloneUser[key] = value;
    this.setState({ user: cloneUser }, () => {
      console.log(this.state.user)
    });
  };
  componentWillReceiveProps(nextProps){
    if(nextProps.svEdited !== null){
      this.setState({user: nextProps.svEdited})
    }
  }
  handleSinhVienSubmit = () => {
    let newUser = { ...this.state.user };
    this.props.handleAddSinhVien(newUser);
  };

  render() {
    return (
      <div className='container'>
        <p className='bg-dark text-light'>Thông tin sinh viên</p>
        <div className='row'>
            <div className='col-6'>
                <div className="form-group">
                  <h4>Mã SV</h4>
                  <input onChange={this.handleGetSinhVienForm} value={this.state.user.id} ref={this.inputRef} type="text" name="id" className="form-control" placeholder="Mã SV" />
                  </div>
            </div>
            <div className='col-6'>
                <h4>Họ Tên</h4>
                  <div className="form-group">
                  <input onChange={this.handleGetSinhVienForm} value={this.state.user.name} ref={this.inputRef} type="text" name="name" className="form-control" placeholder="Họ Tên Sinh Viên" />
                  </div>
            </div>
            <div className='col-6'>
              <h4>Số điện thoại</h4>
                <div className="form-group">
                <input onChange={this.handleGetSinhVienForm} value={this.state.user.sodienthoai} ref={this.inputRef} type="text" name="sodienthoai" className="form-control" placeholder="Số điện thoại" />
                </div>    
            </div>
            <div className='col-6'>
              <h4>Email</h4>
                <div className="form-group">
                <input onChange={this.handleGetSinhVienForm} value={this.state.user.email} ref={this.inputRef} type="text" name="email" className="form-control" placeholder="Email" />
                </div>
            </div>
        </div>
        <button className='btn btn-warning' onClick={this.handleSinhVienSubmit}>Thêm thông tin sinh viên</button>
        <button
          className="btn btn-primary" onClick={() => {
            this.props.handleCapNhatTTSinhVien(
              this.state.user.id,
              this.state.user
            );
          }}
        >
          Cập nhật thông tin Sinh Viên
        </button>
      </div>
    )
  }
}

