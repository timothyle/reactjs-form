import './App.css';
import EX_FORM from './Ex_form/Ex_form';

function App() {
  return (
    <div className="App">
      <EX_FORM/>
    </div>
  );
}

export default App;
